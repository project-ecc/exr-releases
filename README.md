# exr electrum releases

This repo holds the json file that signals to exr wallets what the latest wallet version is.

File format is
{
    "version string" : { "address" : "signed message" }
}

where signed message is the version string signed by the address
